package org.example;

public class Person3 {
    private String firstName;
    private String lastName;
    private int age;

    public Person3(String firstName, String lastName, int age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }

    public String toString() {
        return ("Name: " + firstName + " " + lastName + ", " + age + "years old");
    }
}
